import "water.css";
import "./global.css";

import Main from "./Main.svelte";

const app = new Main({ target: document.body })

export default app;
