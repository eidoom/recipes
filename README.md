# [recipes](https://gitlab.com/eidoom/recipes)

* [Live](https://eidoom.gitlab.io/recipes)

## Usage

### Build

To build and view locally,
```shell
git clone git@gitlab.com:eidoom/recipes.git
cd recipes
source init-venv.sh
./preprocess.py
npm install
npm run build
python3 -m http.server --directory public
```
or run `dev.bash` for a development server.

### Add content

Use `new.py` or copy and edit existing entry in `content/recipes`.

`photo.bash` formats images appropriately.

## Codebase

### Content

Written as YAML files in `content/recipes` and `ingredients`.

### Backend

Processing of data and compilation to JSON format by `preprocess.py`.

* [Python](https://www.python.org/)
* [PyYAML](https://pyyaml.org/)

### Frontend

A statically-compiled SPA from `src`.

* [svelte.js](https://svelte.dev/)
* [routify](https://v3.routify.dev/)
* [water.css](https://watercss.kognise.dev/) ([GitHub](https://github.com/kognise/water.css)) ([npm](https://www.npmjs.com/package/water.css))
* [rollup.js](https://rollupjs.org/guide/en/)

## Licence

The source code is licenced under AGPL-3.0, see `LICENSE`.

The content, ie. the recipes, are licenced under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
