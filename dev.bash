#!/usr/bin/env bash

echo http://localhost:10001

(trap 'kill 0' SIGINT;
[[ -d node_modules ]] || npm i &&
source init-venv.sh &&
while sleep 0.1; do find content/ static/ templates/ preprocess.py dev.bash rollup.config.js -type f | entr -d ./preprocess.py; done &
npm run dev
)
