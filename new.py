#!/usr/bin/env python3

import argparse, pathlib, re, datetime

import yaml

try:
    from yaml import CDumper as Dumper
except ImportError:
    from yaml import Dumper


def get_args():
    parser = argparse.ArgumentParser(description="Create new entry")
    parser.add_argument("title", type=str, help="The entry title")
    parser.add_argument(
        "-k",
        "--kind",
        type=str,
        choices=("recipe", "ingredient"),
        default="recipe",
        help="The kind of entry",
    )
    args = parser.parse_args()
    args.title = args.title.strip()
    return args


def slugify(string):
    return re.sub(r"[^\w-]+", "", str(string).lower().replace(" ", "-"))


if __name__ == "__main__":
    args = get_args()

    if args.kind == "recipe":
        data = {
            "title": args.title,
            "added": datetime.date.today().isoformat(),
            "makes": "this many pieces",
            "ingredients": ["List ingredients here"],
            "method": ["List method steps here"],
        }
    elif args.kind == "ingredient":
        data = {
            "title": args.title,
            "comments": ["List comments here"],
        }

    path = pathlib.Path(f"content/{args.kind}s")
    path.mkdir(exist_ok=True)

    file = path / f"{slugify(data['title'])}.yml"

    if file.is_file():
        print(f"File {file} already exists!")
    else:
        print(f"Creating {file}")
        with open(file, "w") as f:
            yaml.dump(data=data, stream=f, Dumper=Dumper, sort_keys=False)
