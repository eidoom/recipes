#!/usr/bin/env python3

import pathlib, json, re, shutil, string, argparse

import yaml

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

from new import slugify


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-b",
        "--base",
        type=str,
        default="",
        help="Base URL of website (include initial slash)",
    )
    return parser.parse_args()


def process_ingredient(ingredient, recipe_ingredients, all_ingredients):
    try:
        new = re.fullmatch(
            "(?P<quantity>\d+\.?\d*) ?(?P<unit>\w+) ?(?P<name>.*)",
            ingredient,
        ).groupdict()

        new["quantity"] = float(new["quantity"])
        if new["unit"] not in units:
            new["name"] = f"{new['unit']} {new['name']}"
            del new["unit"]
        elif not new["name"]:
            new["name"] = new["unit"]
            del new["unit"]
        if "," in new["name"]:
            new["name"], new["comment"] = new["name"].split(", ", maxsplit=1)
    except AttributeError:
        split = ingredient.split(", ", maxsplit=1)
        if len(split) == 2:
            new = {"name": split[0], "comment": split[1]}
        else:
            new = {"name": ingredient}

    new["slug"] = slugify(new["name"])
    recipe_ingredients.append(new)

    recipe_short = {"title": recipe["title"], "slug": recipe["slug"]}
    try:
        old = next(
            ingredient
            for ingredient in all_ingredients
            if ingredient["slug"] == new["slug"]
        )
        old["recipes"].append(recipe_short)
    except StopIteration:
        all_ingredients.append(
            {
                "name": new["name"],
                "slug": new["slug"],
                "recipes": [recipe_short],
            }
        )


if __name__ == "__main__":
    args = get_args()

    index = string.Template(
        pathlib.Path("templates/index.html").read_text()
    ).substitute(base=args.base)

    public = pathlib.Path("public")
    public.mkdir(exist_ok=True)

    (public / "index.html").write_text(index)

    static = pathlib.Path("static")
    static.mkdir(exist_ok=True)

    shutil.copytree(static, public, dirs_exist_ok=True)

    content = pathlib.Path("content")
    content.mkdir(exist_ok=True)

    with open(content / "units.yml", "r") as f:
        units = yaml.load(stream=f, Loader=Loader)

    recipes = []

    for recipe_path in (content / "recipes").glob("*.yml"):
        with open(recipe_path, "r") as f:
            recipe = yaml.load(stream=f, Loader=Loader)

            recipe["slug"] = slugify(recipe["title"])

            try:
                try:
                    recipe["makes"] = re.fullmatch(
                        "(?P<quantity>\d+\.?\d?) (?P<unit>\w+)", recipe["makes"]
                    ).groupdict()
                except AttributeError:
                    raise Exception(
                        f"Cannot parse makes `{recipe['makes']}` in recipe `{recipe['title']}`"
                    )
                recipe["makes"]["quantity"] = float(recipe["makes"]["quantity"])
            except KeyError:
                pass

            try:
                recipe["images"] = [
                    ",".join(
                        f"{args.base}/"
                        + "/".join(path.parts[1:])
                        + f" {path.stem[len(image)+2:]}w"
                        for path in static.glob(f"img/{image}-w*.avif")
                    )
                    for image in recipe["images"]
                ]
            except KeyError:
                pass

            recipes.append(recipe)

    recipes.sort(key=lambda x: x["title"])

    all_ingredients = []

    for ingredient_path in (content / "ingredients").glob("*.yml"):
        with open(ingredient_path, "r") as f:
            ingredient = yaml.load(stream=f, Loader=Loader)

            ingredient["slug"] = slugify(ingredient["name"])

            ingredient["recipes"] = []

            all_ingredients.append(ingredient)

    for recipe in recipes:
        if isinstance(recipe["ingredients"], list):
            new_ingredients = []
            for ingredient in recipe["ingredients"]:
                process_ingredient(ingredient, new_ingredients, all_ingredients)

        elif isinstance(recipe["ingredients"], dict):
            new_ingredients = {}
            for part, ingredients in recipe["ingredients"].items():
                new_ingredients[part] = []
                for ingredient in ingredients:
                    process_ingredient(
                        ingredient, new_ingredients[part], all_ingredients
                    )

        else:
            raise Exception(f"Cannot parse ingredients in recipe `{recipe['title']}`")

        recipe["ingredients"] = new_ingredients

    all_ingredients.sort(key=lambda x: x["name"])

    src = pathlib.Path("src/routes")
    src.mkdir(exist_ok=True)

    with open(src / "entries/recipes.json", "w") as f:
        json.dump(recipes, f)

    with open(src / "ingredients/ingredients.json", "w") as f:
        json.dump(all_ingredients, f)
