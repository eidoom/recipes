export { capitalise, slugify, addItemToShoppingList };

import { get } from "svelte/store";

import { shoppingList } from "./stores.js";

function capitalise(string) {
  return string.toLowerCase().replace(/^\w/, (c) => c.toUpperCase());
}

function slugify(string) {
  return string
    .toString()
    .toLowerCase()
    .replace(" ", "-")
    .replace(/[^\w-]+/g, "");
}

function addItemToShoppingList(item, persist = true) {
  const i = get(shoppingList).findIndex(
    (e) =>
      e.name === item.name &&
      e.unit === item.unit &&
      e.quantity &&
      item.quantity
  );
  if (i !== -1) {
    shoppingList.update((cur) => {
      cur[i].quantity += item.quantity;
      return cur;
    });
  } else {
    shoppingList.update((cur) => {
      cur.push(item);
      return cur;
    });
  }
  if (persist) {
    localStorage.setItem("shoppingList", JSON.stringify(get(shoppingList)));
  }
}
