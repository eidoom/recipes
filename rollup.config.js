import css from "rollup-plugin-css-only";
import json from "@rollup/plugin-json";
import resolve from "@rollup/plugin-node-resolve";
import svelte from "rollup-plugin-svelte";
import livereload from "rollup-plugin-livereload";
import serve from "rollup-plugin-serve";
import { terser } from "rollup-plugin-terser";

const production = !process.env.ROLLUP_WATCH;

export default {
  input: "src/main.js",
  output: {
    dir: "public",
  },
  plugins: [
    svelte({
      compilerOptions: {
        dev: !production,
      },
    }),
    resolve({
      browser: true,
      dedupe: ["svelte"],
    }),
    json({
      compact: true,
    }),
    css({
      output: "bundle.css",
    }),
    !production && livereload("public"),
    !production &&
      serve({
        contentBase: "public",
        historyApiFallback: true,
        host: "0.0.0.0",
        port: 10001,
      }),
    production && terser(),
  ],
};
