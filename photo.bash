#!/usr/bin/env bash

input=$1
output=$2 # no suffix

for w in 3840 1920 960 480; do
	magick $input -resize $w -quality 25 static/img/$output-w$w.avif
done
