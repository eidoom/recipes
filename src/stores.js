import { writable } from "svelte/store";

export const shoppingList = writable(
  localStorage.getItem("shoppingList")
    ? JSON.parse(localStorage.getItem("shoppingList"))
    : []
);
